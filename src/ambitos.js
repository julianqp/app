
const ambitos = [
  {
    id: 1,
    label: 'Informático'
  },
  {
    id: 2,
    label: 'Deporte'
  },
  {
    id: 3,
    label: 'Medicina'
  },
  {
    id: 4,
    label: 'Social'
  },
  {
    id: 5,
    label: 'Tecnológico'
  },
  {
    id: 6,
    label: 'Económico'
  },
  {
    id: 7,
    label: 'Investigación'
  },
  {
    id: 8,
    label: 'Prácticas'
  },
  {
    id: 9,
    label: 'Restauración'
  },
  {
    id: 10,
    label: 'Ecológico'}
]

export default ambitos;