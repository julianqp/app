import Proyectos from './Principal/Proyectos'
import Mensajes from './Principal/Mensajes'
import Favoritos from './Principal/Favoritos'
import Nuevos from './Principal/Nuevos'
import Homes from './Principal/Homes'
import Ayudas from './Principal/Ayudas'
import Perfiles from './Principal/Perfiles'
import Proyecto from './Components/Proyecto'
import EditarProyecto from './Components/EditarProyecto'

import {
  faUser,
  faPowerOff,
  faFolderOpen,
  faComment,
  faStar,
  faPlusCircle,
  faQuestionCircle,
  faHome
} from "@fortawesome/free-solid-svg-icons";

var routes = [
  {
    path: "",
    name: "Principal",
    icon: faHome,
    component: Homes,
    layout: "/principal"
  },
  {
    path: "/mensajes",
    name: "Mensajes",
    icon: faComment,
    component: Mensajes,
    layout: "/principal"
  },
  {
    path: "/proyectos",
    name: "Proyectos",
    icon: faFolderOpen,
    component: Proyectos,
    layout: "/principal"
  },
  {
    path: "/favoritos",
    name: "Favoritos",
    icon: faStar,
    component: Favoritos,
    layout: "/principal"
  },
  {
    path: "/nuevo",
    name: "Nuevo Proyecto",
    icon:  faPlusCircle,
    component: Nuevos,
    layout: "/principal"
  },
  {
    path: "/ayuda",
    name: "Ayuda",
    icon: faQuestionCircle,
    component: Ayudas,
    layout: "/principal"
  },
  {
    path: "/perfiles",
    name: "Perfil",
    icon: faUser,
    component: Perfiles,
    layout: "/principal"
  },
  {
    path: '/proyecto/:id',
    name: "Proyecto",
    icon: null,
    component: Proyecto,
    layout: "/principal"
  },
  {
    path: '/proyecto/editar/:id',
    name: "Editar",
    icon: null,
    component: EditarProyecto,
    layout: "/principal"
  },
  {
    path: "",
    name: "Cerrar Sesion",
    icon: faPowerOff,
    layout: "/login"
  }
];
export default routes;
