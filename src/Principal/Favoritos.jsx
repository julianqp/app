import React from 'react'
import { TableroEntrada } from '../Components/TableroEntrada'

class Favoritos extends React.Component{
  constructor (props){
    super(props)
    this.state = {
      datos: [],
      load: false
    }
    this.variableDatos = []
  }
  componentDidMount () {
    this._cargaDatos()
  }
  _cargaDatos(){
    const url = 'http://api.local:8000/api/favoritos/'+localStorage.getItem('idActivo')
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          let proyectos = result['data'].map((dato) => {
            return dato.idProyecto
          })
          if(proyectos.length !== 0){
            this._busqueda(proyectos)
          } else{
            this.setState({
              datos: [],
              load: true
            })
          }
        },
        (error) => {
          console.log(error)
        }
      )
  }

  _busqueda(datos){
    let arrayProyectos = []
    let peticiones = datos.map( (idProyecto) =>{
      let url = 'http://api.local:8000/api/proyectos/'+ idProyecto
      return window.fetch(url, {
        method: 'GET',
      }).then((res) => res.json())
        .then(
          (result) => { //  En caso de funcionar.
            arrayProyectos = [...arrayProyectos, result['data']]
          },
          (error) => { // En caso de error.
            console.log('No se ha podido');
          })
    } )
    Promise.all(peticiones).then( () => { this.setState({load: true, datos:arrayProyectos})})
      .catch((error) => console.log(error))

  }

  _tradandoDatos(datos){
    let resultado = <h5 className={'text-center'}>No tienes favoritos</h5>
    if(datos.length !== 0) {
      resultado = datos.map((dato, index) =>
        <TableroEntrada metodo={() => this._cargaDatos() } favoritos={true} key={index} datos={dato}/>)
    }
    return resultado;
  }

  render() {
    let carga = <div className="text-center">
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
    let proyectos = this.state.load
    if(proyectos){
      carga = <div>
        <h1>Favoritos</h1>
        {this._tradandoDatos(this.state.datos)}
      </div>
    }
    return carga;
  }
}
export default Favoritos;