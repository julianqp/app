import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome/index";
import * as PropTypes from "prop-types";

/**
 * Clase que coloca un icono que recibe por props:
 *  clases => Las clases que quieres que lleve el icono (por ejemplo color)
 *  icono => icono que quieres poner (Requerido)
 */

export const Icono = props => {
  const { classes, icon } = props;
  const icono = <FontAwesomeIcon icon={icon} />;
  const clases = classes || "text-black-50";

  return <span className={clases}>{icono}</span>;
};

Icono.propTypes = {
  classes: PropTypes.string,
  icon: PropTypes.object.isRequired
};
