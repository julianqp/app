import React from 'react'
import { Icono } from '../Principal/icono'
import {} from '@fortawesome/free-brands-svg-icons'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import {} from '@fortawesome/free-regular-svg-icons'
import styled from 'styled-components'

class Mensajes extends React.Component {

  render () {
    return <div>
      <h1>Mensajes</h1>
      <div className="row">
        <div className="col-3" style={{overflow: 'scroll'}}>
          <div className={'row w-100'} style={{height: '70px'}}>
            <Icono icon={faSearch} classes={'my-auto mx-auto'} />
            <input className={'my-auto mx-auto w-75 form-control'} type={'text'}/>
          </div>
          <div className={'row'}>
            <div className="list-group w-100 " id="list-tab" role="tablist">
              <a className="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list"
                 href="#list-home" role="tab" aria-controls="home">Home</a>
              <a className="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list"
                 href="#list-home" role="tab" aria-controls="profile">Profile</a>
              <a className="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list"
                 href="#list-home" role="tab" aria-controls="messages">Miguel feo</a>
              <a className="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list"
                 href="#list-home" role="tab" aria-controls="settings">Settings</a>
              <a className="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list"
                 href="#list-home" role="tab" aria-controls="settings">Settings</a>
              <a className="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list"
                 href="#list-home" role="tab" aria-controls="settings">Settings</a>
            </div>
          </div>
        </div>
        <div className="col-9 pl-5">
          <div className="tab-content" id="nav-tabContent">
            <ConversationList/>
          </div>
        </div>
      </div>
    </div>
  }
}

export default Mensajes
const TablonMensaje = styled.main`
  width: 90%
.cartel{
  position: relative;
  height: 100%;
}
.mensaje{
  position: absolute;
  overflow: scroll;
  top: 0;
  left: 2%
  width: 98%
  height: 90%;
  right: 2%
}
.envio{
  width: 100%;
  position: absolute;
  bottom: 0;
}
.ancho{
  width: 90%;
  left: 5%;
  right: 5%;
}
.linea {
  position: absolute;
}

.miMensaje{
  text-right;
  right: 0;
}
.tuMensaje{
  position: static;
  left: 0;
}
`

class ConversationList extends React.Component {

  render () {
    return <div className="tab-pane fade show active" id="list-home" role="tabpanel"
                aria-labelledby="list-home-list">
      <div className={'row'} style={{height: '70px'}}>

          <img className={'my-1'} style={{width: '60px', height: '60px'}}
             src={'http://colegionacional.cl/wp-content/uploads/2017/03/images.png'} alt={'imagen de fondo'}/>

        <p className={'ml-3 my-3'}> Nombre</p>
      </div>
      <div className={'row w-100 '} style={{height: '700px'}}>
        <TablonMensaje>
          <div className={'cartel border'}>
            <div className={'mensaje '}>
              <div className={'mx-3'} >
                <p className={'text-right'}>Buenos diasss</p>
                <p className={'text-left'}>Buenos diass :)</p>
                <p className={'text-right '}>Que tal estas?</p>
                <p className={'text-left '}>Muy bien, y tu?</p>
                <p className={'text-right '}>Pos aqui andamios</p>
                <p className={'text-left '}>Pos ok</p>
                <p className={'text-right'}>Buenos diasss</p>
                <p className={'text-left'}>Buenos diass :)</p>
                <p className={'text-right '}>Que tal estas?</p>
                <p className={'text-left '}>Muy bien, y tu?</p>
                <p className={'text-right '}>Pos aqui andamios</p>
                <p className={'text-left '}>Pos ok</p>
                <p className={'text-right'}>Buenos diasss</p>
                <p className={'text-left'}>Buenos diass :)</p>
                <p className={'text-right '}>Que tal estas?</p>
                <p className={'text-left '}>Muy bien, y tu?</p>
                <p className={'text-right '}>Pos aqui andamios</p>
                <p className={'text-left '}>Pos ok</p>
                <p className={'text-right'}>Buenos diasss</p>
                <p className={'text-left'}>Buenos diass :)</p>
                <p className={'text-right '}>Que tal estas?</p>
                <p className={'text-left '}>Muy bien, y tu?</p>
                <p className={'text-right '}>Pos aqui andamios</p>
                <p className={'text-left '}>Pos ok</p>
              </div>
            </div>
            <div className={'envio'}>
              <hr className={'mb-3'} />
              <div className="input-group mb-3 ancho">
                <input type="text" className="form-control" placeholder="Recipient's username"
                       aria-label="Recipient's username" aria-describedby="button-addon2"/>
                <div className="input-group-append">
                  <button className="btn btn-outline-primary" type="button" id="button-addon2">Enviar</button>
                </div>
              </div>
            </div>
          </div>
        </TablonMensaje>
      </div>
    </div>
  }

}