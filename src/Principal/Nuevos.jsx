import React from 'react'
import styled from 'styled-components'
import Check from '../Components/Check'
import ambitos from '../ambitos'
import { Icono } from './icono'
import { faEdit, faTrashAlt } from '@fortawesome/free-regular-svg-icons'

class Nuevos extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      titulo: '',
      fdonacion: false,
      limiteDonacion: 0,
      cuenta: '',
      colaboracion: false,
      numeroColaboradores: 0,
      ambitos: '',
      objetivos: '',
      descripcion: '',
      sugerencias: '',
      checkeados: [],
      tarea: '',
      tareas: []
    }
  }
  _changeCampo(event){
    this.setState({
      [event.target.id]: event.target.value
    })
  }
  _onChangeCheckeados (checked, id) {
    this.setState((prevState) => {
      const checkeados = checked ? [...prevState.checkeados, id] : prevState.checkeados.filter(x => x !== id)
      return {
        checkeados
      }
    })
  }
  _addTarea(tarea) {
    if(tarea !== ''){
      this.setState((prevState) => {
        return {
          tarea: '',
          tareas: [...prevState.tareas, tarea]
        }
      })
    }
  }
  _editTare(tarea){
    if(tarea !== ''){
      this.setState((prevState) => {
        return {
          tarea: tarea,
          tareas: prevState.tareas.filter(x => x !== tarea)
        }
      })
    }
  }
  _removeTarea(tarea){
    if(tarea !== ''){
      this.setState((prevState) => {
        return {
          tarea: '',
          tareas: prevState.tareas.filter(x => x !== tarea)
        }
      })
    }
  }
  _changeTarea(event){
    this.setState({
      tarea: event.target.value
    })
  }

  _post() {
    console.log('click')
    let {
      titulo,
      fdonacion,
      limiteDonacion,
      cuenta,
      colaboracion,
      objetivos,
      descripcion,
      sugerencias,
      checkeados } = this.state

    if (fdonacion === 'Si'){
      fdonacion = true;
    } else {
      fdonacion = false;
    }

    if (colaboracion === 'Si'){
      colaboracion = true;
    } else {
      colaboracion = false;
    }
    const datos = {
      'titulo': titulo,
      'donacion': fdonacion,
      'donado': 0,
      'colaboracion': colaboracion,
      'descripcion': descripcion,
      'sugerencias': sugerencias,
      'objetivos': objetivos,
      'usuario': localStorage.getItem('Sesion'),
      'ambito': JSON.stringify(checkeados),
      'puntuacion': 0,
      'limiteDonado': limiteDonacion,
      'cuenta': cuenta
    }
    const url = 'http://api.local:8000/api/proyecto'
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos)
    }).then((res) => res.json())
      .then(
        (result) => { //  En caso de funcionar.
          console.log('post tarea')
          this._postTareas(result['data'].id)
        },
        (error) => { // En caso de error.
          this.setState({
            error: error
          })
        })
  }

  _postTareas(id){
    console.log('post tarea')
    const url = 'http://api.local:8000/api/tareas'
     let peticiones = this.state.tareas.map( (tarea) =>{
      let datos = {
         'idProyecto': id,
          'colaboracion': false,
          'descripcion': tarea
      }
      return window.fetch(url, {
        method: 'POST',
        body: JSON.stringify(datos)
      }).then((res) => res.json())
        .then(
          (result) => { //  En caso de funcionar.
          },
          (error) => { // En caso de error.
            console.log('No se ha podido');
          })
    } )
    Promise.all(peticiones).then( () => window.location = 'http://localhost:3000/principal')
      .catch((error) => console.log(error))
  }

  render () {
    let { tarea, tareas } = this.state
    let carga = ''
    if(tareas.length !== 0){
      let total = tareas.map((valor, index) => {
        return <tr key={index}>
          <td>{valor}</td>
          <td>
              <span className={'float-right'} >
                <button onClick={() => this._editTare(valor)} className={'btn mr-5'} key={'editar'}><Icono classes={'text-secondary'} icon={faEdit}/></button>
                <button onClick={() => this._removeTarea(valor)} className={'btn'} key={'borrar'}><Icono classes={'text-danger'} icon={faTrashAlt}/></button>
              </span>
          </td>
        </tr>
      })
      carga = <table className="table table-borderless mt-3 mr-5">
        <tbody>
        {total}
        </tbody>
      </table>
    }
    return <Tablero>
      <div className="border position-relative m-5 ml-5 mr-5 p-5">
        <h5 className={'mb-2'}>Información</h5>
        <div className="media ">
          <img src="https://sistemas.com/termino/wp-content/uploads/Usuario-Icono.jpg" className="mr-4 foto" alt="..."/>
          <div className="media-body">
            <div className="form-row mb-3">
              <div className="col-2">
                <h6>Titulo</h6>
              </div>
              <div className="col-8">
                <input id={'titulo'} onChange={(event) => this._changeCampo(event)} type="text" min="0" className="form-control form-control-sm" placeholder="Titulo del proyecto" />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col-2">
                <h6>Permitir donaciones</h6>
              </div>
              <div className="col-sm-1">
                <select id={'fdonacion'} onChange={(event) => this._changeCampo(event)} className="form-control form-control-sm">
                  <option>No</option>
                  <option>Si</option>
                </select>
              </div>
              <div className="col-2">
                <div className="input-group input-group-sm">
                  <input id={'limiteDonacion'} onChange={(event) => this._changeCampo(event)} type="number" min="0" className="form-control" placeholder={'Cantidad que desea recolectar'} />
                  <div className="input-group-append">
                    <span className="input-group-text">€</span>
                  </div>
                </div>
              </div>
              <div className="col-4">
                <input id={'cuenta'} onChange={(event) => this._changeCampo(event)} type="text" className="form-control form-control-sm" placeholder="Cuenta de paypal" />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col-2">
                <h6>Permitir colaboraciones</h6>
              </div>
              <div className="col-sm-1">
                <select id={'colaboracion'} onChange={(event) => this._changeCampo(event)} className="form-control form-control-sm">
                  <option>No</option>
                  <option>Si</option>
                </select>
              </div>
              <div className="col-2">
                <input id={'numeroColaboradores'} onChange={(event) => this._changeCampo(event)} type="number" min="0" className="form-control form-control-sm" placeholder="Numero de colaboradores" />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col-2">
                <h6>Ámbitos:</h6>
              </div>
            </div>
            <div className="form-row">
              <Ambitos checkeados={this.state.checkeados} cambio={this._onChangeCheckeados.bind(this)}/>
            </div>
          </div>
        </div>
      </div>
      <div className="border position-relative m-5 ml-5 mr-5 p-5">
        <div >
          <TextosConTitulo cambio={this._changeCampo.bind(this)} defaultValue={this.props.objetivos} identificador={'objetivos'} titulo={'Objetivos'} />
        </div>
      </div>
      <div className="border position-relative m-5 ml-5 mr-5 p-5">
        <div >
          <TextosConTitulo cambio={this._changeCampo.bind(this)} defaultValue={this.props.descripcion} identificador={'descripcion'} titulo={'Descripción'} />
        </div>
      </div>
      <div className="border position-relative m-5 ml-5 mr-5 p-5">
        <div>
          <h5 className={'mb-2'}>Tarea</h5>
          <div className="form-row">
            <div className="col-11">
              <input value={tarea} onChange={(event) => this._changeTarea(event)} className="form-control form-control-sm" type="text" placeholder="Añada aqui su tarea" />
            </div>
            <div className="col-1">
              <button onClick={() => this._addTarea(tarea) } className={'btn btn-outline-secondary btn-sm'} >Añadir tarea</button>
            </div>
            {carga}
          </div>
        </div>
      </div>
      <div className="border position-relative m-5 ml-5 mr-5 p-5">
        <div >
          <TextosConTitulo cambio={this._changeCampo.bind(this)} defaultValue={this.props.sugerencias} identificador={'sugerencias'} titulo={'Sugerencias'} />
        </div>
      </div>
      <div className="text-center m-5 ml-5 mr-5 p-5">
        <button className={'btn btn-outline-primary btn-lg btn-block'} onClick={() => this._post()} >Crear</button>
      </div>
    </Tablero>
  }
}
export default Nuevos;

class TextosConTitulo extends React.Component{
  render () {
    let { titulo, cambio, identificador } = this.props

    return <div >
      <h5 className={'mb-2'}>{titulo}</h5>
      <div className="form-group">
        <textarea className="form-control" onChange={(event) => cambio(event) } defaultValue={this.props.valor} id={identificador} rows="5" />
      </div>
    </div>
  }
}

class Ambitos extends React.Component {
  render () {
    let { cambio, checkeados } = this.props
    let seleccion = ambitos.map((valor, index) => {
      let id = valor.id
      let label = valor.label
      return <div key={index} className="col-3">
        <Check label={valor.label} identificador={label}
               checked={checkeados.indexOf(id) !== -1}
               onChange={(event, label) => cambio(event.target.checked, label)} />
      </div>
    })
    return seleccion;
  }
}

const Tablero = styled.main`
#form {
    width: 250px;
    margin: 0 auto;
    height: 50px;
  }
  
  #form p {
    text-align: center;
  }
  
  #form label {
    font-size: 20px;
  }
  
  input[type="radio"] {
    display: none;
  }
  
  label {
    color: grey;
  }
  
  .clasificacion {
    direction: rtl;
    unicode-bidi: bidi-override;
  }
  
  label:hover,
  label:hover ~ label {
    color: orange;
  }
  
  input[type="radio"]:checked ~ label {
    color: orange;
  }
  .botonEditar {
    font-size: 1em;
    position: absolute;
    right: 15px; 
  }
  .botonVer {
    font-size: 1em;
    position: absolute;
    right: 15px;
    bottom: 20px;
  }
  
  .foto{
    width: 140px;
  }
  p {
  margin-top: 0;
  margin-bottom: 0;
  }
  .botones {
    float: rigth;
  }
`