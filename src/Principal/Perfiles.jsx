import React from 'react'
import { Perfil } from '../Components/Perfil'
import { ProyectoPerfil } from '../Components/ProyectoPerfil'
import { ColaboracionPerfil } from '../Components/ColaboracionPerfil'
import { Notificacion } from '../libreria/Notificacion'


class Perfiles extends React.Component{
  constructor (props){
    super(props)
    this.state = {
      datos: null,
      toast: false,
      titulo: '',
      dscripcion: ''
    }
  }
  componentDidMount () {
    this._cargaDatos()
  }

  _cargaDatos(){
    const url = 'http://api.local:8000/api/usuarios/'+localStorage.getItem('idActivo')
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            datos: result
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }

  _cargaDatosModal(){
    const url = 'http://api.local:8000/api/usuarios/'+localStorage.getItem('idActivo')
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            datos: result,
            toast: true,
            titulo: 'Perfil',
            descripcion: 'Su descripcion ha sido actualizada'
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }
  _apiPost(donacion){
    console.log(donacion)
    let datos = {
      'descripcion': donacion.descripcion,
    }
    const url = 'http://api.local:8000/api/usuarios/'+donacion.id
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos)
    }).then((res) => res.json())
      .then(
        (result) => {
          if(result.length !== 0){
            this._cargaDatosModal()
          } else{
            this.setState({
              toast: true,
              titulo: 'Perfil',
              descripcion: 'Usuario no encontrado, pruebe a recargar'
            })
          }
        },
        (error) => {
          this.setState({
            toast: true,
            titulo: 'Perfil',
            descripcion: 'Error en el servidor, recargue de nuevo.'
          })
        }
      )
  }

  render() {
    const { toast } = this.state;
    const handleShow = () => this.setState({ toast: true });
    const handleClose = () => this.setState({ toast: false });
    let carga =  <div>
      <h1>Perfil</h1>
      <div className="text-center">
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
      </div>
    let proyectos = this.state.datos
    if(proyectos !== null){
      carga = <div>
        <h1>Perfil</h1>
        <Notificacion estado={toast} titulo={this.state.titulo} contenido={this.state.descripcion} cerrar={handleClose}/>
        <Perfil notificacion={handleShow} donar={(donacion)=>this._apiPost(donacion)} datos={this.state.datos} />
        <ProyectoPerfil />
        <ColaboracionPerfil />
        </div>
    }
    return carga;
  }
}

export default Perfiles;