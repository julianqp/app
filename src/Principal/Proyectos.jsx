import React from 'react'
import {MisProyectos} from '../Components/MisProyectos'
import { Colaboracion } from '../Components/Colaboracion'
import { Notificacion } from '../libreria/Notificacion'

class Proyectos extends React.Component{
  constructor (props){
    super(props)
    this.state = {
      toast: false,
      fallo: false,
    }
  }

  render() {
    const { toast, fallo } = this.state;
    const handledonacion = () => this.setState({toast: true,});
    const handleCloseDonacion = () => this.setState({ toast: false });
    const handleFallo = () => this.setState({fallo: true,});
    const handleCloseFallo = () => this.setState({ fallo: false });
    //const handleShow = (titulo, contenido) => this.setState({ toast: true, titulo: titulo, contenido: contenido });

    return <div> <h1>Proyectos</h1>
      <Notificacion estado={toast} titulo={'Colaboracion'} contenido={'Su pago se ha realizado correctamente.'} cerrar={handleCloseDonacion}/>
      <Notificacion estado={fallo} titulo={'Colaboracion'} contenido={'Su pago ha fallado.'} cerrar={handleCloseFallo}/>
    <nav>
        <div className="nav nav-tabs" id="nav-tab" role="tablist">
          <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Mis proyectos</a>
          <a className="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Colaboraciones</a>
        </div>
      </nav>
      <div className="tab-content" id="nav-tabContent">
        <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"><MisProyectos /></div>
        <div className="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab"><Colaboracion fallo={handleFallo} notificacion={handledonacion} /></div>
      </div>
    </div>
  }
}

export default Proyectos;