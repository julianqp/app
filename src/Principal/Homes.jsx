import React from 'react'
//import styled from 'styled-components'
import { TableroEntrada } from '../Components/TableroEntrada'
class Homes extends React.Component{
  constructor (props){
    super(props)
    this.state = {
      datos: null,
      favoritos: null,
    }
    localStorage.getItem('idActivo')
  }

  componentDidMount () {
    this._cargaDatos()
    this._buscarFavoritos()
  }

  _buscarFavoritos(){
    const url = 'http://api.local:8000/api/favoritos/'+localStorage.getItem('idActivo')
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          let fav = result['data'].map((dato) => {
            return dato.idProyecto
          })
          this.setState({
            favoritos: fav
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }
  _cargaDatos(){
    const url = 'http://api.local:8000/api/proyectos'
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            datos: result['data']
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }
  _tradandoDatos(datos){
    let resultadoSinDatos = <h4 className={'text-center'}>No hay proyectos existentes</h4>
    let resultadoConDatos = datos.map((dato, index) => {
      return <TableroEntrada favoritos={this.state.favoritos.includes(dato.id)} key={index} datos={dato} />
      })
    return resultadoConDatos.length === 0 ? resultadoSinDatos : resultadoConDatos;
  }

  render() {
    let carga = <div className="text-center">
                  <div className="spinner-border" role="status">
                      <span className="sr-only">Loading...</span>
                  </div>
                </div>
    let { datos, favoritos } = this.state
    if(favoritos !== null &&  datos !== null){
      carga = <div>
        <h1>Proyectos</h1>
        {this._tradandoDatos(this.state.datos)}
      </div>
    }
    return carga
  }
}

export default Homes;