import React from 'react'
import { Toast } from 'react-bootstrap'

export class Notificacion extends React.Component{
  constructor (props){
    super(props)
  }
  render(){
    const fecha = new Date()
    const dia = fecha.getDay()+'/'+fecha.getMonth()+'/'+fecha.getFullYear();
    const hora = fecha.getHours()+':'+fecha.getMinutes();

    const { cerrar, estado, titulo, contenido, icono } = this.props
    return <Toast onClose={() => cerrar()} style={{
      position: "absolute",
      top: 50,
      right: 15
    }} show={estado} delay={4000} autohide>
      <Toast.Header>
        <div className={"p-3 m-2 "+icono}/>
        <strong className="mr-auto">{titulo}</strong>
        <small>{hora+' '+dia}</small>
      </Toast.Header>
      <Toast.Body>
        {contenido}
      </Toast.Body>
    </Toast>
  }
}