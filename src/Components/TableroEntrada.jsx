import React from "react";
import { Link } from "react-router-dom";
import styled from 'styled-components'
import {
  faStar
} from "@fortawesome/free-regular-svg-icons";

import { Icono } from '../Principal/icono'

const Tablero = styled.main`
#form {
    width: 250px;
    margin: 0 auto;
    height: 50px;
  }
  
  #form p {
    text-align: center;
  }
  
  #form label {
    font-size: 20px;
  }
  
  input[type="radio"] {
    display: none;
  }
  
  label {
    color: grey;
  }
  .clasificacion {
    direction: rtl;
    unicode-bidi: bidi-override;
  }
  
  label:hover,
  label:hover ~ label {
    color: orange;
  }
  
  input[type="radio"]:checked ~ label {
    color: orange;
  }
  .botonFavoritos {
    font-size: 1em;
    position: absolute;
    right: 15px; 
  }
  .botonVer {
    font-size: 1em;
    position: absolute;
    right: 15px;
    bottom: 20px;
  }
  
  .foto{
    width: 140px;
  }
  p {
  margin-top: 0;
  margin-bottom: 0;
}
.maginal{
  margin-right: 50px
}
`
export class TableroEntrada extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      favorito: this.props.favoritos
    }

  }

  _addFavoritos (id) {
    let datos = {
      'idProyecto': id,
      'idColaborador': localStorage.getItem('idActivo')
    }
    const url = 'http://api.local:8000/api/favoritos'
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos)
    }).then((res) => res.json())
      .then(
        (result) => {
          this.setState({
              favorito: true
            })
        },
        (error) => {
          console.log(error)
        }
      )
  }
  _deleteFavoritos (id) {
    let url = 'http://api.local:8000/api/favoritos/'+id+'/usuario/'+localStorage.getItem('idActivo')
    window.fetch(url, {
      method: 'POST',
    }).then((res) => res.json())
      .then(
        (result) => {
          if(this.props.metodo){
            this.props.metodo()
          }else {
            this.setState({
              favorito: false
            })
          }
        },
        (error) => {
          console.log(error)
        }
      )
  }

  render () {
    const {datos} = this.props;
    let {
      id,
      usuario,
      titulo,
      ambito,
      puntuacion,
      descripcion
    } = datos;

    let boton
    if(!this.state.favorito){
      boton = <button onClick={() => this._addFavoritos(id)} className="btn btn-outline-warning botonFavoritos">
            Añadir a favoritos <Icono icon={faStar}/>
          </button>
    } else{
      boton = <button onClick={() => this._deleteFavoritos(id)} className="btn btn-outline-danger botonFavoritos">
            Eliminar de favoritos
          </button>
    }
     let ambitosP = 'Vacio'
  if(JSON.parse(ambito).length !== 0){
    ambitosP = JSON.parse(ambito).reduce((acc, current) => acc+', '+current)
  }
    return <Tablero>
      <div className="border position-relative m-5 ml-5 mr-5 p-5">
        <div className="media ">
          {boton}
          <img src="https://sistemas.com/termino/wp-content/uploads/Usuario-Icono.jpg" className="mr-4 foto" alt="..."/>
          <div className="media-body">
            <div className="list-inline">
              <h5 className="mt-0 list-inline-item">Usuario</h5>
              <p className="list-inline-item">{usuario}</p>
            </div>
            <div className="list-inline">
              <h5 className="mt-0 list-inline-item">Proyecto</h5>
              <p className="list-inline-item">{titulo}</p>
            </div>
            <div className="list-inline">
              <h5 className="mt-0 list-inline-item">Ámbito</h5>
              <p className="list-inline-item">{ambitosP}</p>
            </div>
            <div className="list-inline">
              <h5 className="mt-0 list-inline-item">Puntuación</h5>
              <p className="list-inline-item">{puntuacion} / 5</p>
            </div>
          </div>
        </div>
        <Link to={'/principal/proyecto/' + id} className="btn btn-outline-info m-0 botonVer">
          Ver proyecto
        </Link>
        <hr/>
        <div className="mt-2 mr-5">
          <h5>Resumen</h5>
          <p className={'my-2 mr-5'}>{descripcion}</p>
        </div>
      </div>
    </Tablero>;
  }
}