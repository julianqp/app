import React from "react";
import { Icono } from '../Principal/icono'
import {  } from '@fortawesome/free-regular-svg-icons'
import { Link } from "react-router-dom";
import styled from 'styled-components'
import {
  faEdit,
  faTrashAlt
} from "@fortawesome/free-regular-svg-icons"

const Tablero = styled.main`
#form {
    width: 250px;
    margin: 0 auto;
    height: 50px;
  }
  
  #form p {
    text-align: center;
  }
  
  #form label {
    font-size: 20px;
  }
  
  input[type="radio"] {
    display: none;
  }
  
  label {
    color: grey;
  }
  
  .clasificacion {
    direction: rtl;
    unicode-bidi: bidi-override;
  }
  
  label:hover,
  label:hover ~ label {
    color: orange;
  }
  
  input[type="radio"]:checked ~ label {
    color: orange;
  }
  .botonEditar {
    font-size: 1em;
    position: absolute;
    right: 15px; 
  }
  .botonVer {
    font-size: 1em;
    position: absolute;
    right: 15px;
    bottom: 20px;
  }
  
  .foto{
    width: 140px;
  }
  p {
  margin-top: 0;
  margin-bottom: 0;
  }
  .botones {
    float: rigth;
  }
`

class Botones extends React.Component{
  constructor (props){
    super(props)
    this.metodo = this.props.metodo
  }
  render () {
    return <span className={'float-right'} ><Link to={'/principal/proyecto/editar/'+this.props.identificador}  className={'btn mr-5'} key={'editar'}><Icono classes={'text-secondary'} icon={faEdit}/></Link>
     <button onClick={() => this.metodo()} className={'btn'} key={'borrar'}><Icono classes={'text-danger'} icon={faTrashAlt}/></button></span>;
  }
}

export class ProyectoPerfil extends React.Component{
  constructor (props){
    super(props)
    this.state = {
      datos: []
    }
  }
  componentDidMount () {
    this._cargaDatos()
  }

  _deleteProyecto(id){
    const url = 'http://api.local:8000/api/proyecto/'+id
    window.fetch(url, {
      method: 'POST'
    }).then((res) => res.json())
      .then(
        (result) => {
          this._cargaDatos()
        },
        (error) => {
          console.log(error)
        }
      )
  }

  _cargaDatos(){
    const url = 'http://api.local:8000/api/proyectos/usuarios/'+localStorage.getItem('Sesion')
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            datos: result['data']
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }

  render () {
    const { datos } = this.state;
    const listas = datos.map( (proyecto, index) => {
      return <tr key={index}>
        <th scope="row">1</th>
        <td>{proyecto.titulo}</td>
        <td><Botones identificador={proyecto.id} metodo={() => this._deleteProyecto(proyecto.id)} /></td>
      </tr>
    } )
    return <Tablero>
      <div className="border position-relative mt-3 ml-5 mr-5 p-5">
        <div className="media ">
          <div className={'w-100'}>
            <h5>Mis Proyectos</h5>
            <table className="table table-striped">
              <thead>
              <tr key={'cebecera-colaboraciones'}>
                <th scope="col">#</th>
                <th scope="col">Titulo</th>
                <th scope="col"></th>
              </tr>
              </thead>
              <tbody>
              {listas}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </Tablero>;
  }
}