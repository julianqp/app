import React from "react";
import styled from "styled-components";
import Main from "./Main";
import Header from "./Header";

const Maine = styled.section`
.fonfo {
  background-color: #F0F0F7
}
.lateral {
    height: 100%;
    width: 200px !important;
    background-color: white;
    border-right: 1px solid black;
}
.margen{
    margin-left: 200px;
}
.menu{
    top: 45%;
}

.boton-buscar{
    width: 20%;
}
.boton-redondo {
    -webkit-border-radius: 100px;
    -moz-border-radius: 100px;
    border-radius: 100px;
}
.imagen-redonda{
    width: 125px;
    height: 125px;
    border-radius:50%;
}

.sin-estilo {
    text-decoration: none;
}

.imagen {
    padding-top: 5%;
    padding-left: 5%;
    width: 50%;
}
.negro {
    height: 200px;
    padding-left: 13px;
    padding-right: 13px;
}
.h2 {
    margin: 0;
}
`;

const App = () => (
    <Maine>
      <Prueba>
      </Prueba>
    </Maine>

);
const Prueba = () => {
  return [
    <div key={'menu'}
              className={"container lateral position-fixed d-inline"}>
    <h3 className={"d-flex pt-5 justify-content-center"}>Colaborapp</h3>
    <div>
      <div className={"pt-5"}>
        <img
          className={"imagen-redonda d-block mx-auto"}
          src={"https://sistemas.com/termino/wp-content/uploads/Usuario-Icono.jpg"}
          alt="No hay imagen"
        />
      </div>
      <p className={"pt-2 text-center"}>{localStorage.getItem('Sesion')}</p>
    </div>
    <div className={"menu position-absolute"}>
      <Header />
    </div>
  </div>,
    <div
      id={'ventana'}
      key={'contenedor'}
      className="margen">
      <nav className="navbar-light bg-light">
        <div className="form-inline py-2 pl-2 ">
          <input
            className="form-control w-75"
            type="search"
            placeholder="Escriba el contenido que desea buscar"
            aria-label="Buscar"
          />
          <button
            className="boton-redondo boton-buscar btn btn-outline-success mx-auto"
            type="submit"
          >
            Buscar
          </button>
        </div>
      </nav>
      <div className={'ml-3 mt-3 mr-3'}>
        <Main />
      </div>
    </div>]
}

export default App;
