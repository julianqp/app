import React from 'react'
import { Link } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';
import styled from "styled-components";

const LoginSection = styled.main`
   text-align: center;
   .prueba-funcion {
        margin-left: 100px;
   }
`;

class Login  extends React.Component{
  constructor (props){
    super(props)
    this.state = {
      usuario: '',
      pass: '',
      error: ''
    }
  }

  _iniciarSesion(usuario, pass){
    const json = {
      'usuario': usuario,
      'pass': pass,
    }
    const url = 'http://api.local:8000/api/login'
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(json)
    }).then((res) => res.json())
      .then(
        (result) => {
          if (result['data'].length !== 0){
            localStorage.setItem('Sesion', result['data']['usuario'])
            localStorage.setItem('idActivo', result['data']['id'])
            window.location = 'http://localhost:3000/principal'
        } else{
            this.setState({
            error: 'Usuario no encontrado'
          })
          }
        },
        (error) => {
          this.setState({
            error: error
          })
        }
      )
  }
  changeUsuario(event){
    this.setState({
      usuario: event.target.value
    })
  }
  changePas(event){
    this.setState({
      pass: event.target.value
    })
  }

  render () {
    let { usuario, pass } = this.state

      return  <LoginSection>
        <div className={'ml-2'} >
          <p>{this.state.error}</p>
          <input className={'form-control mt-3'} value={usuario} onChange={(event) => this.changeUsuario(event)} type={'text'} placeholder={'Usuario'} />
          <input className={'form-control mt-2'} value={pass} type={'password'} onChange={(event) => this.changePas(event)} placeholder={'Contraseña'} />
          <button className={'form-control btn-dark mt-2'} onClick={() => this._iniciarSesion(usuario, pass)} type={'submit'} > Entrar </ button>
          <Link className={'form-control btn-primary mt-2'} to={'/registro'}> Registro </Link>
          <Link className={'form-control  btn-danger mt-2'} to={'/recuerdo'}> Recordar contraseña </Link>
        </div>
      </LoginSection>

  }
}

export default Login;