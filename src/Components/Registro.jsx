import React from 'react'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import styled from 'styled-components'
const Encapsulado = styled.main`
   text-align: center;
`;

class Registro  extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      nombre: '',
      apellidos: '',
      correo: '',
      usuario:'',
      password1:'',
      password2:'',
      error: ''
    }
  }

  _compruebaContraseña(nuevoUsuario) {
    if (nuevoUsuario.password1 !== nuevoUsuario.password2) {
      this.setState({
        error: 'Las contraseñas no coinciden'
      })
    } else {
      let { nombre, apellidos, correo, usuario, password1 } = nuevoUsuario
      let datosAMandar ={
        'nombre': nombre,
        'apellidos': apellidos,
        'correo': correo,
        'usuario': usuario,
        'pass': password1
      }
      this._registrarUsuario(datosAMandar)
    }
  }

   _registrarUsuario(nuevoUsuario){
    console.log('HOLAAAAA')
     const url = 'http://api.local:8000/api/usuarios'
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(nuevoUsuario)
    }).then((res) => res.json())
      .then(
        (result) => {
          if (result['data'].length !== 0){
            window.location = 'http://localhost:3000/login'
        } else{
            this.setState({
            error: 'Usuario no encontrado'
          })
          }
        },
        (error) => {
          this.setState({
            error: error
          })
        }
      )
  }


  _changeCampo(event){
    this.setState({
      [event.target.id]: event.target.value
    })
  }

  render () {
    let { nombre, apellidos, correo, usuario, password1, password2 } = this.state
    let datosUsuario = {
      'nombre': nombre,
      'apellidos': apellidos,
      'correo': correo,
      'usuario': usuario,
      'password1': password1,
      'password2': password2
    }
    return <Encapsulado>
    <div className={'ml-2'} >
        <p>{this.state.error}</p>
        <input className={'form-control mt-3'} id={'nombre'} value={this.state.nombre} onChange={ (event) => this._changeCampo(event)} type={'text'} placeholder={'Nombre'} />
        <input className={'form-control mt-3'} id={'apellidos'} value={this.state.apellidos} onChange={ (event) => this._changeCampo(event)} type={'text'} placeholder={'Apellidos'} />
        <input className={'form-control mt-3'} id={'correo'} value={this.state.correo} onChange={ (event) => this._changeCampo(event)} type={'text'} placeholder={'Correo'} />
        <input className={'form-control mt-3'} id={'usuario'} value={this.state.usuario} onChange={ (event) => this._changeCampo(event)} type={'text'} placeholder={'Usuario'} />
        <input className={'form-control mt-3'} id={'password1'} value={this.state.password1} onChange={ (event) => this._changeCampo(event)} type={'password'} placeholder={'Contraseña'} />
        <input className={'form-control mt-3'} id={'password2'} value={this.state.password2} onChange={ (event) => this._changeCampo(event)} type={'password'} placeholder={'Repetir contraseña'} />
        <button className={'form-control btn-dark mt-3'} type={'submit'} onClick={() => this._compruebaContraseña(datosUsuario)}> Registrarse </ button>
        <Link className={'form-control btn-primary mt-3'} to={'/login'}> Ya tengo cuenta </Link>
      </div>
    </Encapsulado>
  }
}

export default Registro;