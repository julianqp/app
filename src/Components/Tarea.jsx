import React from 'react'
import { Icono } from '../Principal/icono'
import { faEdit, faTrashAlt } from '@fortawesome/free-regular-svg-icons'

class Tarea extends React.Component {

  render () {
    return <div>
      <h5 className={'mb-2'}>Tarea</h5>
      <AddTarea />
    </div>
  }
}
export default Tarea;

class AddTarea extends React.Component{
  constructor (props){
    super(props)
    this.state = {
      tarea: '',
      tareas: []
    }
  }

  _addTarea(tarea) {
    if(tarea !== ''){
      this.setState((prevState) => {
       return {
         tarea: '',
         tareas: [...prevState.tareas, tarea]
       }
      })
    }
  }

  _editTare(tarea){
     if(tarea !== ''){
      this.setState((prevState) => {
       return {
         tarea: tarea,
         tareas: prevState.tareas.filter(x => x !== tarea)
       }
      })
    }
  }

  _changeTarea(event){
    this.setState({
      tarea: event.target.value
    })
  }

  _removeTarea(tarea){
    if(tarea !== ''){
      this.setState((prevState) => {
       return {
         tarea: '',
         tareas: prevState.tareas.filter(x => x !== tarea)
       }
      })
    }
  }
  render() {
    let { tarea, tareas } = this.state
    let carga = ''
    if(tareas.length !== 0){
      let total = tareas.map((valor, index) => {
        return <tr key={index}>
          <td>{valor}</td>
          <td>
              <span className={'float-right'} >
                <button onClick={() => this._editTare(valor)} className={'btn mr-5'} key={'editar'}><Icono classes={'text-secondary'} icon={faEdit}/></button>
                <button onClick={() => this._removeTarea(valor)} className={'btn'} key={'borrar'}><Icono classes={'text-danger'} icon={faTrashAlt}/></button>
              </span>
          </td>
        </tr>
      })
      carga = <table className="table table-borderless mt-3 mr-5">
            <tbody>
              {carga}
            </tbody>
          </table>
    }

    return<div className="form-row">
        <div className="col-11">
          <input value={tarea} onChange={(event) => this._changeTarea(event)} className="form-control form-control-sm" type="text" placeholder="Añada aqui su tarea" />
        </div>
        <div className="col-1">
          <button onClick={() => this._addTarea(tarea) } className={'btn btn-outline-secondary btn-sm'} >Añadir tarea</button>
        </div>
      {carga}
      </div>

  }
}
