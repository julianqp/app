import React, {Component, Fragment} from "react";
import {  } from '@fortawesome/free-regular-svg-icons'
import styled from 'styled-components'
import { Modal, Acciones, Contenido, Titulo} from '../libreria/modal'
import { Link } from 'react-router-dom'

class ModalDonar extends Component {
  constructor (props) {
    super(props)
    this.onConfirm = props['onConfirm']

    this.state = {
      modalOpen: false,
      buttonDisabled: true,
      cantidad: '',
    }
  }

  /**
   * Metodo que comprobara si los datos están vacios
   * @param cantidad
   * @param checkeados
   * @returns {boolean}
   * @private
   */
  _checkBadData (cantidad) {
    return cantidad === ''
  }

  /**
   * Metodo que cambia el estado de la variable que activa o desactiva el modal
   * @private
   */
  _changeModal () {
    this.setState(prevState => {
      return {
        modalOpen: !prevState.modalOpen
      }
    })
  }

  /**
   * Metodo que formatea los datos para realizar realizar la llamada a la API
   * @private
   */
  _confirm () {
    let carenciaPost = {
        'cantidad': this.state.cantidad,
        'id': this.props.identicador
      }
    this.onConfirm(carenciaPost)
    this._changeModal()
  }
  /**
   * Medodo que actualiza la visibilidad del boton de confirmar, si se ha escrito en el cantidad en carencia y actualiza
   * @param checked
   * @param rasId
   * @private
   */
  _onChangeTexto (cantidad) {
    this.setState(() => {
      return {
        cantidad,
        buttonDisabled: this._checkBadData(cantidad)
      }
    })
  }
  /*
  <textarea className={'form-control col-8'}
              onChange={(event) => this._onChangeTexto(event.target.value)}
              defaultValue={cantidad} style={{ 'height': '10rem' }} />
   */
  render () {
    const { modalOpen, buttonDisabled, cantidad } = this.state
    return <Fragment>
      <button
        className={'btn btn-outline-primary'}
        onClick={() => this._changeModal()}
      >
        Donar
      </button>
      <Modal open={modalOpen} onClose={() => this._changeModal()} maxWidth={'md'} fullWidth>
        <Titulo onClose={() => this._changeModal()}>
          Realizar una donación
        </Titulo>
        <Contenido>

          <h5 className={'mb-4'}>Introduzca la cantidad que desea donar al proyecto</h5>
          <div className={'d-flex justify-content-around mb-2'}>
            <p className={'col-4'}>Cantidad <span className={'text-danger'}>*</span></p>
            <div className="input-group">
                  <input id={'donacion'}
                         onChange={(event) => this._onChangeTexto(event.target.value)}
                         defaultValue={cantidad}
                         type="number" min="0"
                         className="form-control" />
                  <div className="input-group-append">
                    <span className="input-group-text">€</span>
                  </div>
                </div>
          </div>
          <p className={'mb-1'}><span className={'text-danger'}>*</span> Campos obligatorios</p>

        </Contenido>
        <Acciones>
          <button type={'button'} className={'btn btn-outline-success'} onClick={() => this._confirm()} disabled={buttonDisabled}>Confirmar</button>
          <button type={'button'} className={'btn btn-outline-danger'} onClick={() => this._changeModal()}>Cancelar</button>
        </Acciones>
      </Modal>
    </Fragment>
  }
}

class Botones extends React.Component {
   // <button className={'btn btn-outline-primary'} key={'donar'}>Donar</button>
  render () {
    return <div className={'col'}>
      <div className={'d-flex justify-content-center my-3'}><p> 0€ Donados</p></div>
      <div className={'d-flex justify-content-center my-3'}>
        <button className={'btn btn-outline-success'} key={'enviar'}>Enviar mensaje</button>
      </div>
      <div className={'d-flex justify-content-center my-3'}>
          <ModalDonar className={'btn btn-outline-primary'} identicador={this.props.identificador} onConfirm={(donacion) => this.props.donar(donacion)} key={'donar'}  />
      </div>
      <div className={'d-flex justify-content-center my-3'}>
        <button onClick={() => this.props.desapuntarse()} className={'btn btn-outline-danger'} key={'dejar'}>Dejar de
          colaborar
        </button>
      </div>
    </div>;
  }
}

export class Colaboracion extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      datos: '',
      cargado: false
    }
  }
  componentDidMount () {
    this._cargaDatos()
  }

  _cargaDatos(){
    const url = 'http://api.local:8000/api/tareas/colaborador/'+localStorage.getItem('Sesion')
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          let proyectos = result['data'].map((dato) => {
            return dato.idProyecto
          })

          this._busqueda(proyectos)
        },
        (error) => {
          console.log(error)
        }
      )
  }

  _desApuntarse(idTarea){
    let datos = {
      idColaborador: localStorage.getItem('Sesion'),
    }
    const url = 'http://api.local:8000/api/tareas/desapuntarse/'+idTarea
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos)
    }).then((res) => res.json())
      .then(
        (result) => {
          this._cargaDatos()
        },
        (error) => {
          console.log(error)
        }
      )
  }

  _busqueda(datos){
    let respuesta = []
    let peticiones = datos.map( (idProyecto) =>{
      let url = 'http://api.local:8000/api/proyectos/'+ idProyecto
      return window.fetch(url, {
        method: 'GET',
      }).then((res) => res.json())
        .then(
          (result) => { //  En caso de funcionar.
             respuesta = [...respuesta, result['data']]
            return respuesta
          },
          (error) => { // En caso de error.
            console.log('No se ha podido');
          })
    } )
    Promise.all(peticiones).then( () => { this.setState({
      cargado: true,
      datos: respuesta
    })})
      .catch((error) => console.log(error))

  }

  _construirTabla(datos){
    const resultado = datos.map((proyecto, index) => {
      return <tr key={index} >
        <td><TableroEntrada datos={proyecto} /></td>
        <td className={'align-middle'}>
          <Botones
            identificador={proyecto.id}
            donar={(donacion)=>this._apiPost(donacion)}
            desapuntarse={() => this._desApuntarse(proyecto.id)}
          />
        </td>
      </tr>
    })
    return resultado;
  }

  _apiPost(donacion){
    console.log(donacion)
    let datos = {
      'idProyecto': donacion.id,
	    'cantidad' : donacion.cantidad,
      'usuarioDonador': localStorage.getItem('Sesion')
    }
    const url = 'http://api.local:8000/api/donacion'
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos)
    }).then((res) => res.json())
      .then(
        (result) => {
          if(result.length !== 0){
            this.props.notificacion()
          } else{
            this.props.fallo()
          }
        },
        (error) => {
          this.props.fallo()
        }
      )
  }

  render () {
    let carga = '';
    if(!this.state.cargado){
      carga = <div className="d-flex justify-content-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>;
    }else if(this.state.datos.length === 0){
      carga = <div className="alert alert-light text-center m-3" role="alert">
        No colabora en ningún proyecto.
      </div>
    }
    else{
      carga = <table className="table table-borderless">
        <tbody>
        {this._construirTabla(this.state.datos)}
        </tbody>
      </table>;
    }
    return carga
  }
}
const TableroEntrada = props => {
  const { datos } = props;
  let {
    id, usuario,
    titulo,
    ambito,
    puntuacion ,
    descripcion } = datos;

 let ambitosP = 'Vacio'
  if(JSON.parse(ambito).length !== 0){
    ambitosP = JSON.parse(ambito).reduce((acc, current) => acc+', '+current)
  }
  return <Tablero>
    <div className="border position-relative m-5 ml-5 mr-5 p-5">
      <div className="media ">
        <img src="https://sistemas.com/termino/wp-content/uploads/Usuario-Icono.jpg" className="mr-4 foto" alt="..."/>
        <div className="media-body">
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Usuario</h5>
            <p className="list-inline-item">{usuario}</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Proyecto</h5>
            <p className="list-inline-item">{titulo}</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Ámbito</h5>
            <p className="list-inline-item">{ambitosP}</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Puntuación</h5>
            <p className="list-inline-item">{puntuacion} / 5</p>
          </div>
        </div>
      </div>
      <Link to={'/principal/proyecto/'+id} className="btn btn-outline-info botonVer">
        Ver proyecto
      </Link>
      <hr/>
      <div className="mt-3">
        <h5>Resumen</h5>
        <p className={'marginal'}>{descripcion}</p>
      </div>
    </div>
  </Tablero>;
}

const Tablero = styled.main`
#form {
    width: 250px;
    margin: 0 auto;
    height: 50px;
  }
  
  #form p {
    text-align: center;
  }
  
  #form label {
    font-size: 20px;
  }
  
  input[type="radio"] {
    display: none;
  }
  
  label {
    color: grey;
  }
  
  .clasificacion {
    direction: rtl;
    unicode-bidi: bidi-override;
  }
  
  label:hover,
  label:hover ~ label {
    color: orange;
  }
  
  input[type="radio"]:checked ~ label {
    color: orange;
  }
  .botonEditar {
    font-size: 1em;
    position: absolute;
    right: 15px; 
  }
  .botonVer {
    font-size: 1em;
    position: absolute;
    right: 15px;
    bottom: 20px;
  }
  
  .foto{
    width: 140px;
  }
  p {
  margin-top: 0;
  margin-bottom: 0;
  }
  .marginal{
    margin-right: 50px;
  }
  .botones {
    float: rigth;
  }
`