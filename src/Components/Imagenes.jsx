import React from 'react'

class Imagenes extends React.Component {
  constructor (props){
    super(props)

    this.state = {
      input: '',
      imagenes: []
    }
  }

  _subirImagen(){
    this.setState((prevState) => {
      return {
        imagenes: [...prevState.imagenes, this.state.input],
        input: ''
      }
    })
  }

  _volvarImagenes(){
    let salida = this.state.imagenes.map((imagen) =>{
      return <img src={imagen}/>
    })
    return <div className={'w-25'}> {salida} </div>
  }

  _seleccionImagen(event){
    this.setState({
      input: event.target.value
    })
  }
  render () {
    let cargaIMG = this._volvarImagenes()
    let nombre = this.state.input !== '' ? this.state.input : 'Seleccione una imagen';
    return <div>
      <h5 className={'mb-2'}>Imágenes</h5>
      <div className="input-group">
        <div className="custom-file">
          <input type="file" onChange={(event) => this._seleccionImagen(event)}  id="inputGroupFile04" aria-describedby="inputGroupFileAddon04" className="custom-file-input"/>
          <label  className="custom-file-label" for="inputGroupFile04" >{nombre}</label>
        </div>
        <div className="input-group-append">
          <button onClick={() => this._subirImagen()} className="btn btn-outline-secondary" type="button" id="inputGroupFileAddon04">Button</button>
        </div>
      </div>
      {cargaIMG}
    </div>
  }
}

export default Imagenes;