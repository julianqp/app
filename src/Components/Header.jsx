import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import routes from '../routes'
import { Icono } from '../Principal/icono'

const HeaderNav = styled.header`
  a:link {
    text-decoraticon: mome;
  }
  a:hover {
    text-decoration: none;
  }
  a:active {
    text-decoration: underline;
   }
`;

class Header extends React.Component {

  _cierreSesion() {
    window.localStorage.clear()
  }

    _getAllLinks(routes){
      let links = routes.map((prop, key) => {
      if (prop.layout === "/principal") {
        if(prop.icon !== null) {
          return (
            <li style={{listStyle: 'none'}} key={key}>
              <Link className={'list-group-item list-group-item-action'}
                to={prop.layout + prop.path}>
              <Icono icon={prop.icon} classes={'mr-2'}/>
              {prop.name} </Link>
            </li>
          );
        }
      } else if (prop.layout === "/login"){
        return (
          <li onClick={ () => this._cierreSesion()} style={{listStyle: 'none'}} key={key}>
            <Link className={'list-group-item list-group-item-action'}
            to={prop.layout}
          >
            <Icono icon={prop.icon} classes={'mr-2'} />
          {prop.name} </Link>
          </li>
        );
      }
    });
        return links
    }

    render () {
        return <HeaderNav>
        <nav>
            <ul className={'list-group'}>
                {this._getAllLinks(routes)}
            </ul>
        </nav>
    </HeaderNav>
}
}


export default Header;
