import React from "react";
import { Icono } from '../Principal/icono'
import {  } from '@fortawesome/free-regular-svg-icons'
import styled from 'styled-components'
import {
  faTrashAlt
} from "@fortawesome/free-regular-svg-icons"

const Tablero = styled.main`
#form {
    width: 250px;
    margin: 0 auto;
    height: 50px;
  }
  
  #form p {
    text-align: center;
  }
  
  #form label {
    font-size: 20px;
  }
  
  input[type="radio"] {
    display: none;
  }
  
  label {
    color: grey;
  }
  
  .clasificacion {
    direction: rtl;
    unicode-bidi: bidi-override;
  }
  
  label:hover,
  label:hover ~ label {
    color: orange;
  }
  
  input[type="radio"]:checked ~ label {
    color: orange;
  }
  .botonEditar {
    font-size: 1em;
    position: absolute;
    right: 15px; 
  }
  .botonVer {
    font-size: 1em;
    position: absolute;
    right: 15px;
    bottom: 20px;
  }
  
  .foto{
    width: 140px;
  }
  p {
  margin-top: 0;
  margin-bottom: 0;
  }
  .botones {
    float: rigth;
  }
`
class Botones extends React.Component{
  render(){
    return <button onClick={() => this.props.desapuntarse()} className={'btn float-right'} key={'editar'}><Icono
      classes={'text-danger'} icon={faTrashAlt}/></button>;
  }
}

export class ColaboracionPerfil extends React.Component{
  constructor (props){
    super(props)
    this.state = {
      datos: []
    }
  }
  componentDidMount () {
    this._cargaDatos()
  }

  _cargaDatos(){
    const url = 'http://api.local:8000/api/tareas/colaborador/'+localStorage.getItem('Sesion')
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          let proyectos = result['data'].map((dato) => {
            return dato.idProyecto
          })
          this._busqueda(proyectos)
        },
        (error) => {
          console.log(error)
        }
      )
  }

  _busqueda(datos){
    let respuesta = []
    let peticiones = datos.map( (idProyecto) =>{
      let url = 'http://api.local:8000/api/proyectos/'+ idProyecto
      return window.fetch(url, {
        method: 'GET',
      }).then((res) => res.json())
        .then(
          (result) => { //  En caso de funcionar.
            respuesta = [...respuesta, result['data']]
              return  respuesta
          },
          (error) => { // En caso de error.
            console.log('No se ha podido');
          })
    } )
    Promise.all(peticiones).then( () => {
      this.setState({
      datos: respuesta
    })})
      .catch((error) => console.log(error))
  }

  _desApuntarse(idTarea){
    let datos = {
      idColaborador: localStorage.getItem('Sesion'),
    }
    const url = 'http://api.local:8000/api/tareas/desapuntarse/'+idTarea
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos)
    }).then((res) => res.json())
      .then(
        (result) => {
          this._cargaDatos()
        },
        (error) => {
          console.log(error)
        }
      )
  }

 render () {
   const {datos} = this.state;
   let listas = <h5 className={'text-center'}>No participas en ningun proyecto </h5>
   if (datos.length !== 0) {
     let misDatos = datos.map((proyecto, index) => {
       return <tr key={index}>
         <th scope="row">1</th>
         <td>{proyecto.titulo}</td>
         <td>Tareas</td>
         <td>Donaciones</td>
         <td><Botones desapuntarse={() => this._desApuntarse(proyecto.id)}/></td>
       </tr>
     })
     listas = <table className="table table-striped">
       <thead>
       <tr key={'cabecera'}>
         <th scope="col">#</th>
         <th scope="col">Titulo</th>
         <th scope="col">Tarea</th>
         <th scope="col">Donación</th>
         <th scope="col"></th>
       </tr>
       </thead>
       <tbody>
       {misDatos}
       </tbody>
     </table>
   }

   return <Tablero>
     <div className="border position-relative mt-3 ml-5 mr-5 p-5">
       <div className="media ">
         <div className={'w-100'}>
           <h5>Mis colaboraciones</h5>
             {listas}
         </div>
       </div>
     </div>
   </Tablero>;
 }
}