import React from "react";
import { Icono } from '../Principal/icono'
import {  } from '@fortawesome/free-regular-svg-icons'
import styled from 'styled-components'
import {
  faEdit,
  faTrashAlt
} from "@fortawesome/free-regular-svg-icons"
import { Link } from 'react-router-dom'

class Botones extends React.Component {

  render () {
    return <span className={'float-right'}><Link to={'proyecto/editar/' + this.props.identificafor} className={'btn mr-5'} key={'editar'}><Icono
      classes={'text-secondary'} icon={faEdit}/></Link>
     <button onClick={() => this.props.metodo()} className={'btn'} key={'borrar'}><Icono
       classes={'text-danger'} icon={faTrashAlt}/></button></span>;
  }
}

export class MisProyectos extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      datos: ''
    }
  }
  componentDidMount () {
    this._cargaDatos()
  }

  _cargaDatos(){
    const url = 'http://api.local:8000/api/proyectos/usuarios/'+localStorage.getItem('Sesion')
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            datos: result['data']
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }

  _deleteProyecto(id){
    const url = 'http://api.local:8000/api/proyecto/'+id
    window.fetch(url, {
      method: 'POST'
    }).then((res) => res.json())
      .then(
        (result) => {
          this._cargaDatos()
        },
        (error) => {
          console.log(error)
        }
      )
  }

  _construirTabla(datos){
    const resultado = datos.map((proyecto, index) => {
          return <tr key={index} >
                    <td><TableroEntrada datos={proyecto} /></td>
                    <td className={'align-middle'}><Botones metodo={() => this._deleteProyecto(proyecto.id)} identificafor={proyecto.id}/></td>
                </tr>
    })
    return resultado;
  }

render () {
    let carga = '';
    if(this.state.datos === ''){
      carga = <div className="d-flex justify-content-center">
                  <div className="spinner-border" role="status">
                       <span className="sr-only">Loading...</span>
                  </div>
              </div>;
    }else if(this.state.datos.length === 0){
      carga = <div className="alert alert-light text-center m-3" role="alert">
        No tiene ningún proyecto creado.
      </div>
    } else{
      carga = <table className="table table-borderless">
                <tbody>
                    {this._construirTabla(this.state.datos)}
                </tbody>
              </table>;
    }
    return carga
  }
}

const TableroEntrada = props => {
   const { datos } = props;
  let { id,
        usuario,
        titulo,
        ambito,
        puntuacion ,
        descripcion } = datos;
 let ambitosP = 'Vacio'
  if(JSON.parse(ambito).length !== 0){
    ambitosP = JSON.parse(ambito).reduce((acc, current) => acc+', '+current)
  }
  return <Tablero>
    <div className="border position-relative m-5 ml-5 mr-5 p-5">
      <div className="media ">
        <img src="https://sistemas.com/termino/wp-content/uploads/Usuario-Icono.jpg" className="mr-4 foto" alt="..."/>
        <div className="media-body">
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Usuario</h5>
            <p className="list-inline-item">{usuario}</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Proyecto</h5>
            <p className="list-inline-item">{titulo}</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Ámbito</h5>
            <p className="list-inline-item">{ambitosP}</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Puntuación</h5>
            <p className="list-inline-item">{puntuacion} / 5</p>
          </div>
        </div>
      </div>
      <Link to={'/principal/proyecto/'+id} className="btn btn-outline-info botonVer ">
        Ver proyecto
      </Link>
      <hr/>
      <div className="mt-3">
        <h5>Resumen</h5>
        <p className={'pr-5'}>{descripcion}</p>
      </div>
    </div>
  </Tablero>;
  }

  const Tablero = styled.main`
#form {
    width: 250px;
    margin: 0 auto;
    height: 50px;
  }
  
  #form p {
    text-align: center;
  }
  
  #form label {
    font-size: 20px;
  }
  
  input[type="radio"] {
    display: none;
  }
  
  label {
    color: grey;
  }
  
  .clasificacion {
    direction: rtl;
    unicode-bidi: bidi-override;
  }
  
  label:hover,
  label:hover ~ label {
    color: orange;
  }
  
  input[type="radio"]:checked ~ label {
    color: orange;
  }
  .botonEditar {
    font-size: 1em;
    position: absolute;
    right: 15px; 
  }
  .botonVer {
    font-size: 1em;
    position: absolute;
    right: 15px;
    bottom: 20px;
  }
  
  .foto{
    width: 140px;
  }
  p {
  margin-top: 0;
  margin-bottom: 0;
  }
  .botones {
    float: rigth;
  }
`