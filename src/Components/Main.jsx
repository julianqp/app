import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import styled from "styled-components";
import routes from '../routes'

const MainSection = styled.main`
    padding: 10px 20px;
   
`;
class Main extends React.Component {
    getRoutes(routes) {
    return routes.map((prop, index) => {
      if (prop.layout === "/principal" ) {
        return (
          <Route key={index}
            exact path={prop.layout + prop.path}
            component={prop.component}
          />
        );
      } else if (prop.layout === "/login") {
        return (
          <Redirect key={index} to={'/principal'} from={'/login'} />
        )
      }
      else {
        return null;
      }
    });
  };
  render () {
    let rutas = this.getRoutes(routes);
    return <MainSection>
        <Switch key={'enlaces'}>
          {rutas}
        </Switch>
    </MainSection>
  }
}

export default Main;
