import React from 'react'
import * as PropTypes from 'prop-types'

class Check extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isChecked: props.checked,
      label: props.label
    }
  }

  _toggleCheckboxChange (event) {
    this.props.onChange(event, this.props.identificador)
    this.setState(() => (
      {
        isChecked: !this.state.isChecked
      }
    ))
  }

  render () {
    let { label, isChecked } = this.state

     let salida = <label className='input-group mb-3 pointer' >
        <div className='input-group-prepend'>
          <div className='input-group-text'>
            <input type='checkbox' checked={isChecked} onChange={(event) => this._toggleCheckboxChange(event)} aria-label='Checkbox for following text input' />
          </div>
        </div>
        <p className='form-control form-control-sm size-ras' aria-label='Text input with checkbox'> {label} </p>
      </label>
    return salida
  }
}

Check.propTypes = {
  label: PropTypes.string.isRequired
  // handleCheckboxChange: PropTypes.func.isRequired
}

export default Check
