import React from 'react'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import styled from 'styled-components'

const Encapsulado = styled.main`
   text-align: center;
`;

class Recuerdo  extends React.Component{
  _comprobar(){

  }
  render () {
    return <Encapsulado>
      <form className={'ml-2'} action={"/login"}>
          <p>Introduzca el correo para restablecer la contraseña</p>
          <input className={'form-control mt-3'} type={'text'} placeholder={'correo'} />
          <button className={'form-control btn-dark mt-2'} onClick={this._comprobar.bind(this)} type={'submit'} > Recordar </ button>
          <Link className={'form-control btn-primary mt-2'} to={'/login'}> Atras </Link>
      </form>
    </Encapsulado>
  }
}

export default Recuerdo;