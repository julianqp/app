import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import { MDBNotification } from "mdbreact";
import { Notificacion }from '../libreria/Notificacion'
import { Acciones, Contenido, Modal, Titulo } from '../libreria/modal'
import Textarea from '@material-ui/core/InputBase/Textarea'

const Tablero = styled.main`
#form {
    width: 250px;
    margin: 0 auto;
    height: 50px;
  }
  #form p {
    text-align: center;
  }
  #form label {
    font-size: 20px;
  }
  input[type="radio"] {
    display: none;
  }
  label {
    color: grey;
  }
  .clasificacion {
    direction: rtl;
    unicode-bidi: bidi-override;
  }
  label:hover,
  label:hover ~ label {
    color: orange;
  }
  input[type="radio"]:checked ~ label {
    color: orange;
  }
  .botonEditar {
    font-size: 1em;
    position: absolute;
    right: 15px; 
  }
  .botonVer {
    font-size: 1em;
    position: absolute;
    right: 15px;
    bottom: 20px;
  }
  .foto{
    width: 140px;
  }
  p {
  margin-top: 0;
  margin-bottom: 0;
}
`
export class Perfil extends React.Component{
  constructor (props){
    super(props)
  }

  render(){
    const {datos} = this.props;
    let {
      nombre,
      apellidos,
      correo,
      usuario,
      numProyecto,
      descripcion
    } = datos['data'];
    // https://mdbootstrap.com/docs/react/advanced/notifications/

    return<Tablero>
      <div className="border position-relative mt-5 ml-5 mr-5 p-5">
        <div className="media ">
          <button className="btn btn-outline-secondary botonEditar">
            Editar
          </button>
          <img src="https://sistemas.com/termino/wp-content/uploads/Usuario-Icono.jpg" className="mr-4 foto" alt="..."/>
          <div className="media-body">
            <div className="list-inline">
              <h5 className="mt-0 list-inline-item">Nombre</h5>
              <p className="list-inline-item">{nombre}</p>
            </div>
            <div className="list-inline">
              <h5 className="mt-0 list-inline-item">Apellidos</h5>
              <p className="list-inline-item">{apellidos}</p>
            </div>
            <div className="list-inline">
              <h5 className="mt-0 list-inline-item">Correo</h5>
              <p className="list-inline-item">{correo}</p>
            </div>
            <div className="list-inline">
              <h5 className="mt-0 list-inline-item">Usuario</h5>
              <p className="list-inline-item">{usuario}</p>
            </div>
            <div className="list-inline">
              <h5 className="mt-0 list-inline-item">Numero de proyectos</h5>
              <p className="list-inline-item">{numProyecto}</p>
            </div>
          </div>
        </div>
      </div>
      <div className="border position-relative mt-3 ml-5 mr-5 p-5">
        <div className="media ">
          <ModalDescripcion className={''} identicador={this.props.identificador} onConfirm={(donacion) => this.props.donar(donacion)} key={'donar'}  />
          <div>
            <h5 className={'mb-3'}>Descripción</h5>
            <p className={'pr-3'}>{descripcion}</p>
          </div>
        </div>
      </div>
    </Tablero>;
  }
}

class ModalDescripcion extends Component {
  constructor (props) {
    super(props)
    this.onConfirm = props['onConfirm']

    this.state = {
      modalOpen: false,
      buttonDisabled: true,
      descripcion: this.props.descripcion ? this.props.descripcion: ''
    }
  }

  /**
   * Metodo que comprobara si los datos están vacios
   * @param descripcion
   * @param checkeados
   * @returns {boolean}
   * @private
   */
  _checkBadData (descripcion) {
    return descripcion === ''
  }

  /**
   * Metodo que cambia el estado de la variable que activa o desactiva el modal
   * @private
   */
  _changeModal () {
    this.setState(prevState => {
      return {
        modalOpen: !prevState.modalOpen
      }
    })
  }

  /**
   * Metodo que formatea los datos para realizar realizar la llamada a la API
   * @private
   */
  _confirm () {
    let nuevaDescripcion = {
      'descripcion': this.state.descripcion,
      'id': localStorage.getItem('idActivo')
    }
    this.onConfirm(nuevaDescripcion)
    this._changeModal()
  }
  /**
   * Medodo que actualiza la visibilidad del boton de confirmar, si se ha escrito en el descripcion en carencia y actualiza
   * @param checked
   * @param rasId
   * @private
   */
  _onChangeTexto (descripcion) {
    this.setState(() => {
      return {
        descripcion,
        buttonDisabled: this._checkBadData(descripcion)
      }
    })
  }
  /*
  <textarea className={'form-control col-8'}
              onChange={(event) => this._onChangeTexto(event.target.value)}
              defaultValue={descripcion} style={{ 'height': '10rem' }} />
   */
  render () {
    const { modalOpen, buttonDisabled, descripcion } = this.state
    return <Fragment>
      <button
        className={'btn btn-outline-secondary botonEditar'}
        onClick={() => this._changeModal()}
      >
        Editar
      </button>
      <Modal open={modalOpen} onClose={() => this._changeModal()} maxWidth={'md'} fullWidth>
        <Titulo onClose={() => this._changeModal()}>
          Editar descripción
        </Titulo>
        <Contenido>
          <h5 className={'mb-4'}>Introduzca su descripción</h5>
          <div className={'d-flex justify-content-around mb-2'}>
            <div className="input-group">
              <textarea id={'donacion'}
                        onChange={(event) => this._onChangeTexto(event.target.value)}
                        defaultValue={descripcion}
                        rows="5"
                        className="form-control m-4" >
              </textarea>
            </div>
          </div>
        </Contenido>
        <Acciones>
          <button type={'button'} className={'btn btn-outline-success'} onClick={() => this._confirm()} disabled={buttonDisabled}>Confirmar</button>
          <button type={'button'} className={'btn btn-outline-danger'} onClick={() => this._changeModal()}>Cancelar</button>
        </Acciones>
      </Modal>
    </Fragment>
  }
}