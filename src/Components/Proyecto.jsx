import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import { Notificacion } from '../libreria/Notificacion'
import { Acciones, Contenido, Modal, Titulo } from '../libreria/modal'
import {
  faStar
} from "@fortawesome/free-regular-svg-icons";

import { Icono } from '../Principal/icono'



class ModalDonar extends Component {
  constructor (props) {
    super(props)
    this.onConfirm = props['onConfirm']

    this.state = {
      modalOpen: false,
      buttonDisabled: true,
      cantidad: '',
    }
  }

  /**
   * Metodo que comprobara si los datos están vacios
   * @param cantidad
   * @param checkeados
   * @returns {boolean}
   * @private
   */
  _checkBadData (cantidad) {
    return cantidad === ''
  }

  /**
   * Metodo que cambia el estado de la variable que activa o desactiva el modal
   * @private
   */
  _changeModal () {
    this.setState(prevState => {
      return {
        modalOpen: !prevState.modalOpen,
        cantidad: ''
      }
    })
  }

  /**
   * Metodo que formatea los datos para realizar realizar la llamada a la API
   * @private
   */
  _confirm () {
    let carenciaPost = {
      'cantidad': this.state.cantidad,
      'id': this.props.identificador
    }
    this.onConfirm(carenciaPost)
    this._changeModal()
  }
  /**
   * Medodo que actualiza la visibilidad del boton de confirmar, si se ha escrito en el cantidad en carencia y actualiza
   * @param checked
   * @param rasId
   * @private
   */
  _onChangeTexto (cantidad) {
    this.setState(() => {
      return {
        cantidad,
        buttonDisabled: this._checkBadData(cantidad)
      }
    })
  }
  /*
  <textarea className={'form-control col-8'}
              onChange={(event) => this._onChangeTexto(event.target.value)}
              defaultValue={cantidad} style={{ 'height': '10rem' }} />
   */
  render () {
    const { modalOpen, buttonDisabled, cantidad } = this.state
    return <Fragment>
      <button className={'btn btn-outline-secondary botonDonar'}
        onClick={() => this._changeModal()}
      >
        Donar
      </button>
      <Modal open={modalOpen} onClose={() => this._changeModal()} maxWidth={'md'} fullWidth>
        <Titulo onClose={() => this._changeModal()}>
          Realizar una donación
        </Titulo>
        <Contenido>

          <h5 className={'mb-4'}>Introduzca la cantidad que desea donar al proyecto</h5>
          <div className={'d-flex justify-content-around mb-2'}>
            <p className={'col-4'}>Cantidad <span className={'text-danger'}>*</span></p>
            <div className="input-group">
              <input id={'donacion'}
                     onChange={(event) => this._onChangeTexto(event.target.value)}
                     defaultValue={cantidad}
                     type="number" min="0"
                     className="form-control" />
              <div className="input-group-append">
                <span className="input-group-text">€</span>
              </div>
            </div>
          </div>
          <p className={'mb-1'}><span className={'text-danger'}>*</span> Campos obligatorios</p>

        </Contenido>
        <Acciones>
          <button type={'button'} className={'btn btn-outline-success'} onClick={() => this._confirm()} disabled={buttonDisabled}>Confirmar</button>
          <button type={'button'} className={'btn btn-outline-danger'} onClick={() => this._changeModal()}>Cancelar</button>
        </Acciones>
      </Modal>
    </Fragment>
  }
}

class Proyecto extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      favorito: '',
      datos: '',
      toast: false,
      fallo: false,
      dona: false,
      donaf: false,
    }
    this.handleClose = () => this.setState({ toast: false });
    this.handleShowFallo = () => this.setState({ fallo: true });
    this.handleCloseFallo = () => this.setState({ fallo: false });
    this.handleDonacionShow = () => this.setState({ dona: true });
    this.handleDonacionNoShow = () => this.setState({ dona: false });
    this.handleDonacionfShow = () => this.setState({ donaf: true });
    this.handleDonacionfNoShow = () => this.setState({ donaf: false });
  }
  componentDidMount () {
    this._cargaDatos()
  }

  _cargaDatos(){
    const url = 'http://api.local:8000/api/proyectos/' + this.props.match.params.id
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            datos: result['data']
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }
  _buscarFavoritos(){
    const url = 'http://api.local:8000/api/favoritos/'+localStorage.getItem('idActivo')+'/'+this.props.match.params.id
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          if (result['data'].length !== 0) {
            this.setState({
              favorito: true,
            })
          } else{
            this.setState({
              favorito: false,
            })
          }
        },
        (error) => {
          console.log(error)
        }
      )
  }


  _apiPost(donacion){
    console.log(donacion)
    let datos = {
      'idProyecto': donacion.id,
      'cantidad' : donacion.cantidad,
      'usuarioDonador': localStorage.getItem('Sesion')
    }
    const url = 'http://api.local:8000/api/donacion'
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos)
    }).then((res) => res.json())
      .then(
        (result) => {
          if(result.length !== 0){
            this.handleDonacionShow()
          } else{
            this.handleDonacionfShow()
          }
        },
        (error) => {
          this.handleDonacionfShow()
        }
      )
  }

  render () {
    const { toast, fallo, dona, donaf } = this.state;
    const handleShow = () => this.setState({ toast: true });

    let carga = '';
    if(this.state.datos === '' || this.state.favorito){
      carga = <div className="d-flex justify-content-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>;
    } else{

      let { ambito,
        descripcion,
        donado,
        id,
        limiteDonado,
        objetivos,
        puntuacion,
        sugerencias,
        titulo,
        usuario } = this.state.datos;

  let ambitosP = 'Vacio'
  if(JSON.parse(ambito).length !== 0){
    ambitosP = JSON.parse(ambito).reduce((acc, current) => acc+', '+current)
  }
      carga = <Tablero>
        <Notificacion estado={toast} icono={'bg-success'} titulo={'Proyecto'} contenido={'Se ha apuntado correctamente en la tarea.'} cerrar={this.handleClose}/>
        <Notificacion estado={fallo} icono={'bg-danger'} titulo={'Proyecto'} contenido={'Fallo al inscribirse en la tarea.'} cerrar={this.handleCloseFallo}/>
        <Notificacion estado={dona} icono={'bg-success'} titulo={'Colaboracion'} contenido={'Su pago se ha realizado correctamente.'} cerrar={this.handleDonacionNoShow}/>
        <Notificacion estado={donaf} icono={'bg-danger'} titulo={'Colaboracion'} contenido={'Su pago ha fallado.'} cerrar={this.handleDonacionfNoShow}/>
    <h1>Proyecto</h1>
    <div className="border position-relative m-5 ml-5 mr-5 p-5">
      <div className="media ">
        <ModalDonar
                    identificador={id}
                    onConfirm={(donacion)=>this._apiPost(donacion)}
                    key={'donar'}  />
        <BotonFavorito identidicador={id} fav={this.state.favorito} />
        <img src="https://sistemas.com/termino/wp-content/uploads/Usuario-Icono.jpg" className="mr-4 foto" alt="..."/>
        <div className="media-body">
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Usuario</h5>
            <p className="list-inline-item">{usuario}</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Proyecto</h5>
            <p className="list-inline-item">{titulo}</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Ámbito</h5>
            <p className="list-inline-item">{ambitosP}</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Puntuación</h5>
            <p className="list-inline-item">{puntuacion} / 5</p>
          </div>
          <div className="list-inline">
            <h5 className="mt-0 list-inline-item">Recaudación</h5>
            <p className="list-inline-item">{donado} / {limiteDonado} €</p>
          </div>
        </div>
      </div>
    </div>
    <div className="border position-relative m-5 ml-5 mr-5 p-5">
      <div className="media-body">
        <h5 className="mb-1">Objetivos</h5>
        <p>{objetivos}</p>
      </div>
    </div>
    <div className="border position-relative m-5 ml-5 mr-5 p-5">
      <div className="media-body">
        <h5 className="mb-3">Descripción</h5>
        <p>{descripcion}</p>
      </div>
    </div>
    <div className="border position-relative m-5 ml-5 mr-5 p-5">
      <div className="media-body">
        <h5 className="mb-3">Tareas</h5>
        <Tarea fallo={this.handleShowFallo} notificacion={handleShow} idTarea={id}/>
      </div>
    </div>

    <div className="border position-relative m-5 ml-5 mr-5 p-5">
      <div className="media-body">
        <h5 className="mb-3">Sugerencias</h5>
        <p>{sugerencias}</p>
      </div>
    </div>
  </Tablero>;
    }
    return carga
  }
}

class BotonFavorito extends React.Component {
  constructor (props){
    super(props)
    this.state = {
      favorito: this.props.fav
    }
    this.id = this.props.identidicador
  }

     _addFavoritos (id) {
    let datos = {
      'idProyecto': id,
      'idColaborador': localStorage.getItem('idActivo')
    }
    const url = 'http://api.local:8000/api/favoritos'
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos)
    }).then((res) => res.json())
      .then(
        (result) => {
          this.setState({
              favorito: true
            })
        },
        (error) => {
          console.log(error)
        }
      )
  }
  _deleteFavoritos (id) {
    let url = 'http://api.local:8000/api/favoritos/'+id+'/usuario/'+localStorage.getItem('idActivo')
    window.fetch(url, {
      method: 'POST',
    }).then((res) => res.json())
      .then(
        (result) => {
          if(this.props.metodo){
            this.props.metodo()
          }else {
            this.setState({
              favorito: false
            })
          }
        },
        (error) => {
          console.log(error)
        }
      )
  }
  render(){
    let boton
    if(!this.state.favorito){
      boton = <button onClick={() => this._addFavoritos(this.id)} className="btn btn-outline-warning botonFavoritos">
            Añadir a favoritos <Icono icon={faStar}/>
          </button>
    } else{
      boton = <button onClick={() => this._deleteFavoritos(this.id)} className="btn btn-outline-danger botonFavoritos">
            Eliminar de favoritos
          </button>
    }
    return boton
  }
}

class Tarea extends React.Component{
  constructor (props) {
    super(props)
    this.state = {
      datos: ''
    }
  }

  componentDidMount () {
    this._cargaTareas()
  }

  _cargaTareas(){
   const url = 'http://api.local:8000/api/tareas/' + this.props.idTarea
    window.fetch(url, {
      method: 'GET'
    }).then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            datos: result['data']
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }

  _apuntarse(idTarea){
    let datos = {
      idColaborador: localStorage.getItem('Sesion'),
      colaboracion: true
    }
    const url = 'http://api.local:8000/api/tareas/apuntarse/'+idTarea
    window.fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos)
    }).then((res) => res.json())
      .then(
        (result) => {
          this.props.notificacion()
          this._cargaTareas()
        },
        (error) => {
          this.props.fallo()
        }
      )
  }

  render (){
    let datos = <h4 className={'text-center'} >No hay tareas</h4>
    if(this.state.datos.length !== 0 ){
      datos = this.state.datos.map( (tarea, index) => {
        let colabo = tarea.colaboracion ? tarea.idColaborador : <button onClick={() => this._apuntarse(tarea.id)} className={'btn btn-outline-dark'}>Apuntarse</button>
        return <tr key={index}>
          <td scope="row" >{tarea.descripcion}</td>
          <td>{colabo}</td>
        </tr>
      })
      return <table className="table table-hover">
      <thead>
      <tr>
        <th scope="col" className="col-10">Descripción de la tarea</th>
        <th scope="col" >Colaborador</th>
      </tr>
      </thead>
      <tbody>
      {datos}
      </tbody>
    </table>
    }
    return datos
  }
}

const Tablero = styled.main`
#form {
    width: 250px;
    margin: 0 auto;
    height: 50px;
  }
  
  #form p {
    text-align: center;
  }
  
  #form label {
    font-size: 20px;
  }
  
  input[type="radio"] {
    display: none;
  }
  
  label {
    color: grey;
  }
  
  .clasificacion {
    direction: rtl;
    unicode-bidi: bidi-override;
  }
  .botonFavoritos {
    font-size: 1em;
    position: absolute;
    right: 15px; 
  }
  
  label:hover,
  label:hover ~ label {
    color: orange;
  }
  
  input[type="radio"]:checked ~ label {
    color: orange;
  }
  .botonEditar {
    font-size: 1em;
    position: absolute;
    right: 15px; 
  }
  .botonDonar{
    font-size: 1em;
    position: absolute;
    left: 35%;
    bottom: 20%;
  }
  
  .botonVer {
    font-size: 1em;
    position: absolute;
    right: 15px;
    bottom: 20px;
  }
  
  .foto{
    width: 140px;
  }
  p {
  margin-top: 0;
  margin-bottom: 0;
  }
  .botones {
    float: rigth;
  }
`
export default Proyecto;