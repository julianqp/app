import React from "react";
import { BrowserRouter as Router, Switch, Redirect, Route } from "react-router-dom";

import App from "./Components/App";
import Login from "./Components/Login";
import Recuerdo from "./Components/Recuerdo";
import Registro from "./Components/Registro";
import 'bootstrap/dist/css/bootstrap.css';
import styled from "styled-components";
import { Icono } from './Principal/icono'
import {
faFacebookF,
  faInstagram
} from "@fortawesome/free-brands-svg-icons";
import {
  faEnvelope
} from "@fortawesome/free-regular-svg-icons";
import {
  faPhone
} from "@fortawesome/free-solid-svg-icons";

const Encapsulado = styled.main`
  .fondo {
    height: 100vh;
    background: -webkit-linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
    background-size: 200% 200%;
    -webkit-animation: Gradient 10s ease infinite;
    -moz-animation: Gradient 10s ease infinite;
    animation: Gradient 10s ease infinite;
    .derecha {
    margin-left: 25%;
}
@-webkit-keyframes Gradient {
    0% {
        background-position: 0% 50%
    }
    50% {
        background-position: 100% 50%
    }
    100% {
        background-position: 0% 50%
    }
}
@-moz-keyframes Gradient {
    0% {
        background-position: 0% 50%
    }
    50% {
        background-position: 100% 50%
    }
    100% {
        background-position: 0% 50%
    }
}
@keyframes Gradient {
    0% {
        background-position: 0% 50%
    }
    50% {
        background-position: 100% 50%
    }
    100% {
        background-position: 0% 50%
    }
}
.alineado {
    width: 25%;
    top: 50px;
    left: 50%
}
.pading{
    margin-top: 15vh;
}
.padingmas{
    margin-top: 18vh;
}
.foter{
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    color: white;
    text-align: center;
    background-color:#35353569;
}

.foter p{
    margin: 1px;
}

`;

class Aplicacion extends React.Component{

render () {
return <Router key={'router'}>
          <Switch>
              <Route key={'app'} path={'/principal'}>
              <App />
            </Route>
              <Route key={'login'}  path={'/login'}>
                    <Encapsulado className={"position-relative"}>
                      <div className={'fondo'}>
                        <img className={"derecha pading mr-5 w-25"} src="IMG/logo.png" alt=""/>
                        <div className={"position-absolute padingmas ml-5 alineado"}>
                        <Login />
                      </div>
                      <footer className="foter">
                        <div className="row mt-2 d-flex justify-content-around">
                          <p><Icono icon={faFacebookF}/></p>
                          <p><Icono icon={faInstagram}/></p>
                          <p><Icono icon={faPhone}/> 666 666 666 </p>
                          <p><Icono icon={faEnvelope}/> correo@correo.es</p>
                        </div>
                        <hr/>
                        <p>Copyright &copy; Julián Querol Polo</p>
                      </footer>
                      </div>
                    </Encapsulado>
                  </Route>
              <Route key={'registro'}  path={'/registro'}>
                    <Encapsulado className={"position-relative"}>
                      <div className={'fondo'}>
                      <img className={"derecha pading mr-5 w-25"} src="IMG/logo.png" alt=""/>
                      <div className={"position-absolute padingmas ml-5 alineado"}>
                      <Registro />
                      </div>
                      <footer className="foter">
                          <div className="row mt-2 d-flex justify-content-around">
                            <p><Icono icon={faFacebookF}/></p>
                            <p><Icono icon={faInstagram}/></p>
                            <p><Icono icon={faPhone}/> 666 666 666 </p>
                            <p><Icono icon={faEnvelope}/> correo@correo.es</p>
                          </div>
                        <hr/>
                        <p>Copyright &copy; Julián Querol Polo</p>
                      </footer>
                      </div>
                    </Encapsulado>
                  </Route>
              <Route key={'recuerdo'}  path={'/recuerdo'}>
                    <Encapsulado className={"position-relative"}>
                      <div className={'fondo'}>
                      <img className={"derecha pading mr-5 w-25"} src="IMG/logo.png" alt=""/>
                      <div className={"position-absolute padingmas ml-5 alineado"}>
                      <Recuerdo />
                      </div>
                      <footer className="foter">
                        <div className="row mt-2 d-flex justify-content-around">
                          <p><Icono icon={faFacebookF}/></p>
                          <p><Icono icon={faInstagram}/></p>
                          <p><Icono icon={faPhone}/> 666 666 666 </p>
                          <p><Icono icon={faEnvelope}/> correo@correo.es</p>
                        </div>
                        <hr/>
                        <p>Copyright &copy; Julián Querol Polo</p>
                      </footer>
                      </div>
                    </Encapsulado>
                  </Route>
              <Redirect key={'remplazo'}  from={'/'} to={'/login'}>
            </Redirect>
          </Switch>
      </Router>
  }
}
export default Aplicacion;